# -*- coding: utf-8 -*-
# © Odoo SA (code reused from account_bank_statement_import)
# (originally licensed as LGPL)
# © 2017 Le Filament (<http://www.le-filament.com>)
# License GPL-3.0 or later (http://www.gnu.org/licenses/gpl.html).

from odoo import api, models, _
from odoo.exceptions import UserError


class AccountBankStatementImport(models.TransientModel):
    _inherit = 'account.bank.statement.import'

    @api.multi
    def auto_import_file(self, file_to_import):
        # Let the appropriate implementation module parse the file
        # and return the required data
        currency_code, account_number, stmts_vals = self._parse_file(
            open(file_to_import, 'r').read())
        # Check raw data
        super(AccountBankStatementImport, self)._check_parsed_data(stmts_vals)
        # Try to find the journal in odoo based on account number
        journal = self.env['account.journal'].search(
            [('bank_acc_number', '=like', account_number)], limit=1)
        if not journal:
            raise UserError(
                _("No journal found matching '%s'.") % account_number)
        # Prepare statement data to be used for bank statements creation
        stmts_vals = super(
            AccountBankStatementImport,
            self)._complete_stmts_vals(stmts_vals, journal, account_number)
        # Create the bank statements
        super(AccountBankStatementImport, self)\
            ._create_bank_statements(stmts_vals)
        # Now that the import worked out, set it as the bank_statements_source
        # of the journal
        journal.bank_statements_source = 'file_import'

    def _parse_file(self, data_file):
        ofx = super(AccountBankStatementImport, self)._check_ofx(data_file)
        if not ofx:
            return super(AccountBankStatementImport, self)._parse_file(
                data_file)

        currency_code, account_number, stmts_vals = super(
            AccountBankStatementImport, self)._parse_file(data_file)

        try:
            available_balance = float(ofx.account.statement.available_balance)
            stmts_vals[0]['balance_start'] = (stmts_vals[0]['balance_start'] -
                                              stmts_vals[0]['balance_end_real']
                                              + available_balance)
            stmts_vals[0]['balance_end_real'] = available_balance
        except NameError:
            raise UserError(_('Attribute "available_balance" not available \
                              in OFX file'))

        return currency_code, account_number, stmts_vals
